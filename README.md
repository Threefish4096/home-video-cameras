A small program to manage home cameras by using [Contacam](https://www.contaware.com/) via "API".

---

The [ONVIF](https://www.onvif.org/) protocol was used for the program through the use of [TP-LINK_Tapo_C200_cameras](https://www.tp-link.com/it/home-networking/cloud-camera/tapo-c200/).

Because of the product forwards traffic to China, after the configuration I decided to block it with specific rules on my home firewall, restricting the connection to the [TP-Link_Tapo_application](https://play.google.com/store/apps/details?id=com.tplink.iot&hl=en&gl=US).

---

python-telegram-bot API package used:
 `pip3 install python-telegram-bot==21.4`

The python version can be updated to the latest one at will.
Strings to put in contacam:
 `C:\Windows\System32\cmd.exe`

In each video camera the command to set:
 `/C "D:\execute_python_sala.bat" %full%`


## License
GNU General Public License v3.0 or later License.

---