import sys
import os
import asyncio
from telegram import Bot
from telegram.error import TelegramError

# AUTHORIZED CHAT ID
nan = 
gaet = 

# Your bot token
token = ''

# Create the Bot instance
bot = Bot(token=token)

async def send_video():
    video = str(sys.argv[1])

    # Check if the file exists
    if not os.path.isfile(video):
        print(f"Error: File '{video}' not found.")
        sys.exit(1)

    # Send the video
    try:
        with open(video, 'rb') as video_file:
            await bot.send_video(chat_id=nan, video=video_file, caption='Movement in ', supports_streaming=True)
            await bot.send_video(chat_id=gaet, video=video_file, caption='Movement in ', supports_streaming=True)
    except TelegramError as e:
        print(f"Telegram API error: {e}")
    except Exception as e:
        print(f"An unexpected error occurred: {e}")

if __name__ == "__main__":
    # Run the asynchronous function
    asyncio.run(send_video())
